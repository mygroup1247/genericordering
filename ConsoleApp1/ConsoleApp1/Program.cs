﻿using System.Linq.Expressions;

namespace ConsoleApp1
{
    class Program
    {
        public static void Main(string[] args)
        {

            var list = new List<Human>()
        {
            new Human(){Name="John",Age=12,Surname="B"},
            new Human(){Name="Bob",Age= 32,Surname="C"},
            new Human(){Name="Serg",Age=5,Surname="A"},
        };
            const string sortOption = "Name"; //requst data

            var props = typeof(Human).GetProperties();
            List<Human> result = new List<Human>();

            //var a = typeof(Human).GetMembers().Where(m => m.Name == sortOption).First();
            var prop = typeof(Human).GetProperty(sortOption);
            var test = prop.Name.ToString();
            result = Get<Human>(list.AsQueryable(), ex => ex);
            if (props.Where(x => x.Name == sortOption) != null)
            {
                if (sortOption == "Name")
                {
                    result = Get<Human>(list.AsQueryable(), ex => ex.Name);
                }
                else if (sortOption == "Surname")
                {
                    result = Get<Human>(list.AsQueryable(), ex => ex.Surname);
                }
                else if (sortOption == "Age")
                {
                    result = Get<Human>(list.AsQueryable(), ex => ex.Age);
                }
            }

            #region FhirParse
            /* var patients = new List<ClinicPatient>();

             var bundleOptions = new JsonSerializerOptions().ForFhir(typeof(Bundle).Assembly);

             var patientOptions = new JsonSerializerOptions().ForFhir(typeof(Patient).Assembly);

             DirectoryInfo dir = new DirectoryInfo(Directory.GetParent("../../../../").FullName + "\\seeds");
             var files = dir.GetFiles();

             foreach (var file in files)
             {
                 if (file.Exists)
                 {
                     var jsonbundle = File.ReadAllText(file.FullName);
                     var bandle = JsonSerializer.Deserialize<Bundle>(jsonbundle, bundleOptions);
                     var patient = JsonSerializer.Deserialize<Patient>(bandle.Entry[0].Resource.ToJson(), patientOptions);

                     ClinicPatient clinicPatient = new ClinicPatient()
                     {
                         Id = patient.Id.ToString(),
                         Name = patient.Name.First().GivenElement.First().ToString(),
                         Surname = patient.Name.First().Family.ToString(),
                         AddressLine = "City:" + patient.Address.First().City.ToString() + " Address: " + patient.Address.First().LineElement.First().Value.ToString(),
                         BirthDate = DateOnly.Parse(patient.BirthDate.ToString()),
                         Gender = patient.Gender.ToString(),
                         Phone = patient.Telecom.First().Value.ToString(),
                         Deceased = patient?.Deceased == null ? null : DateTime.Parse(patient.Deceased.First().Value.ToString()),
                         Language = patient.Communication.First().Language.Text.ToString()

                     };
                     patients.Add(clinicPatient);
                 }

             }
             Console.WriteLine(patients);*/
            #endregion
        }

        public static List<TEntity> Get<TEntity>(IQueryable<TEntity> humans, params Expression<Func<TEntity, object>>[] expresison)
        {
            var result = humans.OrderBy(expresison.First());
            return result.ToList();
        }

    }

    public class Human
    {
        public string Name { get; set; }

        public int Age { get; set; }

        public string Surname { get; set; }
    }
}

